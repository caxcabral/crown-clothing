import React from 'react';
import MenuItem from '../menu-item/menu-item.component';

import './directory.styles.scss';
import sections from './directory.data';

class Directory extends React.Component {
    constructor() {
        super();
        this.state = {
            collections: sections
        };
    }

    render() {
        return (            
            <div className='directory-menu'>
                {
                    this.state.collections.map(( {id, ...otherProps}) => (
                            <MenuItem 
                                key={id} 
                                {...otherProps}
                            />
                    ))
                }
            </div>            
        );
    }
}

export default Directory;