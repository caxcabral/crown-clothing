import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


const config = {
    apiKey: "AIzaSyBLazhmClFsqSt5lQPfUJjov4N0MAJI2aw",
    authDomain: "crwn-db-2b6d3.firebaseapp.com",
    databaseURL: "https://crwn-db-2b6d3.firebaseio.com",
    projectId: "crwn-db-2b6d3",
    storageBucket: "crwn-db-2b6d3.appspot.com",
    messagingSenderId: "400507776804",
    appId: "1:400507776804:web:a1e9d785277b39ad2b4db1",
    measurementId: "G-R2BVNHT1D2"
};

export const createUserProfileDocument = async ( userAuth, additionalData) => {
    if (!userAuth) return;

    const userRef = firestore.doc(`/users/${userAuth.uid}`);
    const snapShot = await userRef.get();

    if (!snapShot.exists) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();
        
        try {
            await userRef.set( {
                displayName,
                email,
                createdAt,
                ...additionalData
            });
        } catch (error) {
                console.log('Error creating user', error.message);
            }
    }
    return userRef;

}

firebase.initializeApp(config);


export const auth=firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({prompt: 'select_account'});
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;

